//
//  AutoLayoutSessionAppDelegate.h
//  AutolayoutSession
//
//  Created by shashi kumar on 5/16/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutoLayoutSessionAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
