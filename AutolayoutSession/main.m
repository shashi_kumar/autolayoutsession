//
//  main.m
//  AutolayoutSession
//
//  Created by shashi kumar on 5/16/14.
//  Copyright (c) 2014 HCS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AutoLayoutSessionAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AutoLayoutSessionAppDelegate class]));
    }
}
